// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace testapplication.ios
{
    [Register("HomeViewController")]
    partial class HomeViewController
    {
        [Outlet]
        UIKit.UITableView HomeTable { get; set; }

        void ReleaseDesignerOutlets()
        {
            if (HomeTable != null)
            {
                HomeTable.Dispose();
                HomeTable = null;
            }
        }
    }
}
