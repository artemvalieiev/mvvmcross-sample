using System;
using MvvmCross.iOS.Views;
using testapplication.ViewModel;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace testapplication.ios
{
    [MvxFromStoryboard("Login")]
    public partial class LoginViewController : MvxViewController<LoginViewModel>
    {
        private int startSize = 14;

        public LoginViewController() { }
        public LoginViewController(IntPtr handle) : base(handle) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var bindingSet = this.CreateBindingSet<LoginViewController, LoginViewModel>();
            bindingSet.Bind(LoginTextField).For(textField => textField.Text)
                      .To(vm => vm.Login).Mode(MvvmCross.Binding.MvxBindingMode.TwoWay);
            bindingSet.Bind(PasswordTextField).To(vm => vm.Password);
            bindingSet.Bind(LoginButton).To(vm => vm.LoginCommand);
            bindingSet.Bind(TitleLabel).To(vm => vm.Title).OneWay();
            bindingSet.Bind(LoginButton).For("Title").To(vm => vm.ButtonTitle);
            bindingSet.Bind(LoginButton).For(button => button.Enabled)
                      .To(vm => vm.IsLoginEnabled);
            bindingSet.Apply();

            LoginButton.TouchUpInside += (s, e) => OnTextChanged(s, e);
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            LoginTextField.Font = LoginTextField.Font.WithSize(startSize++);
        }
    }
}
