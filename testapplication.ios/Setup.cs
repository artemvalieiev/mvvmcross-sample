﻿using System;
using MvvmCross.iOS.Platform;
using testapplication.ios.Bootstrap;
using UIKit;
namespace testapplication.ios
{
    public class Setup : MvxIosSetup
    {
        public Setup(IMvxApplicationDelegate appDelegate, UIWindow window) : base(appDelegate, window) { }

        protected override MvvmCross.Core.ViewModels.IMvxApplication CreateApp()
        {
            return new App();
        }

        public override void LoadPlugins(MvvmCross.Platform.Plugins.IMvxPluginManager pluginManager)
        {
            base.LoadPlugins(pluginManager);
            pluginManager.EnsurePluginLoaded<UserInteractionPluginBootstrap>();
        }
    }
}
