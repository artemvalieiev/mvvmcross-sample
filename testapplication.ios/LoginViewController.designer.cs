// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace testapplication.ios
{
    [Register("LoginViewController")]
    partial class LoginViewController
    {
        [Outlet]
        UIKit.UIButton LoginButton { get; set; }

        [Outlet]
        UIKit.UITextField LoginTextField { get; set; }

        [Outlet]
        UIKit.UITextField PasswordTextField { get; set; }

        [Outlet]
        UIKit.UILabel TitleLabel { get; set; }

        void ReleaseDesignerOutlets()
        {
            if (LoginButton != null)
            {
                LoginButton.Dispose();
                LoginButton = null;
            }

            if (LoginTextField != null)
            {
                LoginTextField.Dispose();
                LoginTextField = null;
            }

            if (PasswordTextField != null)
            {
                PasswordTextField.Dispose();
                PasswordTextField = null;
            }

            if (TitleLabel != null)
            {
                TitleLabel.Dispose();
                TitleLabel = null;
            }
        }
    }
}
