﻿using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;

namespace testapplication.ios
{
    public partial class HomeTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("HomeTableViewCell");
        public static readonly UINib Nib;

        static HomeTableViewCell()
        {
            Nib = UINib.FromName("HomeTableViewCell", NSBundle.MainBundle);
        }

        protected HomeTableViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var bindingSet = this.CreateBindingSet<HomeTableViewCell, string>();
                bindingSet.Bind(MainLabel).To(vm => vm);
                bindingSet.Apply();
            });
        }
    }
}
