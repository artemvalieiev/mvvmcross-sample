﻿using System;
using MvvmCross.Core.ViewModels;
using testapplication.ViewModel;

namespace testapplication
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();

            RegisterAppStart<LoginViewModel>();
        }
    }
}