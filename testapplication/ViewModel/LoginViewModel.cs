﻿using System;
using MvvmCross.Core.ViewModels;
using System.Threading.Tasks;
using Chance.MvvmCross.Plugins.UserInteraction;
using System.Linq;
using PropertyChanged;
using MvvmCross.Core.Navigation;

namespace testapplication.ViewModel
{
    [AddINotifyPropertyChangedInterfaceAttribute]
    public class LoginViewModel : MvxViewModel
    {
        private readonly IUserInteraction userInteraction;
        private readonly IMvxNavigationService navigationService;

        public string Login { get; set; }
        public string Password { get; set; }

        public string Title => Login + " " + Password;
        public string ButtonTitle => $"Let's go {Login}";

        public bool IsLoginEnabled => !(string.IsNullOrEmpty(Login) || string.IsNullOrEmpty(Password));

        public MvxAsyncCommand LoginCommand { get; set; }

        public LoginViewModel(IUserInteraction userInteraction, IMvxNavigationService navigationService)
        {
            this.userInteraction = userInteraction;
            this.navigationService = navigationService;
            this.LoginCommand = new MvxAsyncCommand(LoginAsync);
        }

        private Task LoginAsync() => navigationService.Navigate<HomeViewModel, string>(Login);
    }
}
