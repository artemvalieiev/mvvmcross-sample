﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Binding.ExtensionMethods;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace testapplication.ViewModel
{
    public class HomeViewModel : MvxViewModel<string>
    {
        public string Title { get; set; }

        public IEnumerable<string> Items { get; set; }

        public override Task Initialize(string parameter)
        {
            Title = parameter;

            var wordCount = parameter?.ToCharArray().Count() ?? 10;
            Items = Enumerable.Range(0, wordCount).Select(i => parameter.Substring(0, i));

            return Task.FromResult(true);
        }
    }
}
