﻿
using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using testapplication.ViewModel;

namespace testapplication.droid
{
    [Activity(Label = "HomeActivity")]
    public class HomeActivity : MvxActivity<HomeViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Home);
        }
    }
}
