using Android.Content;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using testapplication.droid.Bootstrap;

namespace testapplication.droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp() => new App();

        protected override IMvxTrace CreateDebugTrace() => new DebugTrace();

        public override void LoadPlugins(MvvmCross.Platform.Plugins.IMvxPluginManager pluginManager)
        {
            base.LoadPlugins(pluginManager);
            pluginManager.EnsurePluginLoaded<UserInteractionPluginBootstrap>();
        }
    }
}
