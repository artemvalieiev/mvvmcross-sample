﻿using System;
using testapplication.ViewModel;
using MvvmCross.Droid.Views;
using Android.App;
namespace testapplication.droid
{
    [Activity()]
    public class LoginActivity : MvxActivity<LoginViewModel>
    {
        protected override void OnCreate(Android.OS.Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login);
        }
    }
}
